/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const { Router } = require('express');
const jsonServer = require('json-server');

const middlewares = jsonServer.defaults();
const router = Router();
router.use(middlewares);
router.use(jsonServer.bodyParser);

const adapter = new FileSync(path.join(__dirname, 'db.json'));
const db = low(adapter);

const paginate = (queryString, key, searchKey = 'name') => {
  const { size, orderBy, order, search } = queryString.query;

  let { page } = queryString.query;
  if (!page) {
    page = 0;
  }

  const query = db.get(key).cloneDeep();
  const filtered = query.orderBy(orderBy, order).filter((row) => row[searchKey].search(search) >= 0);

  const offset = page * size;
  const content = filtered.drop(offset).slice(0, size);

  return {
    totalElements: filtered.size(),
    content,
    pageable: { pageNumber: Number(page) },
  };
};

router.get('/front/findAllOrders', (req, res) => {
  return res.send(paginate(req, 'orders', 'name'));
});

router.post('/front/createOrders', (req, res) => {
  db.get('orders').push(req.body).write();
  return res.send(req.body);
});

router.use(jsonServer.router(path.join(__dirname, 'db.json')));

module.exports = router;
