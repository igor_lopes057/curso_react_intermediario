/* eslint-disable no-plusplus */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const faker = require('faker/locale/pt_BR');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const writeToJSON = (key, items) => {
  const adapter = new FileSync(path.join(__dirname, 'db.json'));
  const db = low(adapter);

  db.set(key, items).write();
};

const orders = [];
const createOrders = () => {
  for (let i = 0; i < 10; i++) {
    orders.push({
      id: i + 1,
      name: faker.name.firstName(),
      lastName: faker.name.lastName(),
      phone: faker.phone.phoneNumber(),
      order: faker.random.arrayElement(['Pizza Calabresa', 'Pizza 4 Queijos', 'Pizza Portuguesa']),
      description: faker.datatype.boolean() ? 'Sem cebola' : null,
    });
  }
};

createOrders();

writeToJSON('orders', orders);
