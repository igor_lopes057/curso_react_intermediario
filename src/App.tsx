import React from 'react';
import { LandingPage } from 'pages/LandingPage';
import { ToastProvider } from 'context/ToastContext';
import { Toast } from 'components/Toast';

function App() {
  return (
    <ToastProvider>
      <LandingPage />
      <Toast />
    </ToastProvider>
  );
}

export default App;
