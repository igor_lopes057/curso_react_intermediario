import { AlertColor } from '@mui/material';
import React, { createContext, ReactNode, useState, useCallback } from 'react';

interface ToastProviderProps {
  children: ReactNode;
}

interface ToastContextData {
  isOpen: boolean;
  severityColor: AlertColor | undefined;
  message: string;
  handleClose: () => void;
  onMessageSuccess: (ToastMessage: string) => void;
  onMessageError: (ToastMessage?: string) => void;
  onMessageWarning: (ToastMessage: string) => void;
}

export const ToastContext = createContext<ToastContextData>({} as ToastContextData);

export const ToastProvider = ({ children }: ToastProviderProps): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);
  const [message, setMessage] = useState('');
  const [severityColor, setSeverityColor] = useState<AlertColor | undefined>(undefined);

  const handleClose = () => {
    setIsOpen(false);
  };

  const onMessageSuccess = useCallback((ToastMessage: string) => {
    setIsOpen(true);
    setMessage(ToastMessage);
    setSeverityColor('success');
  }, []);

  const onMessageError = useCallback(
    (ToastMessage = 'Ocorreu um erro. Por favor aguarde ou tente recarregar a página.') => {
      setIsOpen(true);
      setMessage(ToastMessage);
      setSeverityColor('error');
    },
    []
  );

  const onMessageWarning = useCallback((ToastMessage: string) => {
    setIsOpen(true);
    setMessage(ToastMessage);
    setSeverityColor('warning');
  }, []);

  return (
    <ToastContext.Provider
      value={{ isOpen, severityColor, message, onMessageSuccess, onMessageError, onMessageWarning, handleClose }}
    >
      {children}
    </ToastContext.Provider>
  );
};
