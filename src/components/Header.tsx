import React from 'react';
import { AppBar, Toolbar, Grid, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  toolbar: {
    padding: '0 1.5rem',
    flexWrap: 'inherit',
  },
  contentToolbar: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      marginLeft: 'auto',
    },
  },
}));

export const Header = (): JSX.Element => {
  const classes = useStyles();

  return (
    <AppBar position="static" color="primary" elevation={0}>
      <Toolbar className={classes.toolbar}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          style={{ flexWrap: 'inherit' }}
        >
          Pizzaria
        </Grid>
      </Toolbar>
    </AppBar>
  );
};
