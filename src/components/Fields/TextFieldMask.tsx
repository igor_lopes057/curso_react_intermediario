import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { FieldProps } from 'formik';
import React from 'react';
import NumberFormat from 'react-number-format';

const useStyles = makeStyles({
  input: {
    borderRadius: '4px',
    marginBottom: '8px',
  },
});

export const TextFieldMask = ({ field, form: { errors }, ...props }: FieldProps): JSX.Element => {
  const classes = useStyles();

  const format = field.value.replace(/\D/g, '').length <= 10 ? '(##) ####-#####' : '(##) #####-####';

  return (
    <NumberFormat
      format={format}
      {...field}
      {...props}
      error={!!errors[field.name]}
      helperText={errors[field.name]}
      classes={{ root: classes.input }}
      customInput={TextField}
    />
  );
};
