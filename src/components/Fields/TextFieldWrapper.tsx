import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { FieldProps } from 'formik';
import React from 'react';

const useStyles = makeStyles({
  input: {
    borderRadius: '4px',
    marginBottom: '8px',
  },
});

export const TextFieldWrapper = ({ field, form: { errors }, ...props }: FieldProps): JSX.Element => {
  const classes = useStyles();

  return (
    <TextField
      {...field}
      {...props}
      error={!!errors[field.name]}
      helperText={errors[field.name]}
      classes={{ root: classes.input }}
    />
  );
};
