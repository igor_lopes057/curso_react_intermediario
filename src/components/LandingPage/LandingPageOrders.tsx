import React from 'react';
import { Typography, Box, Stack, Button, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { User } from 'types/orders';

interface LandingPageOrdersProps {
  onButtonClick: () => void;
  data: User[];
  totalElements: number;
}

const useStyles = makeStyles(() => ({
  comment: {
    lineHeight: '2.3rem',
    color: '#5b5b5b',
    fontWeight: 600,
  },
  card: {
    background: '#F0FFF0',
    width: '300px',
    borderRadius: '8px',
    boxShadow: '1px 2px 0px rgba(0,0,0, 0.25)',
    minHeight: '175px',
  },
  userInfo: {
    padding: '1rem',
    background: '#5B2F8B',
    borderTopLeftRadius: '8px',
    borderTopRightRadius: '8px',
  },
}));

export const LandingPageOrders = ({ data, totalElements, onButtonClick }: LandingPageOrdersProps): JSX.Element => {
  const classes = useStyles();

  return (
    <>
      <Grid container alignItems="center" justifyContent="space-around" sx={{ margin: '2rem 0' }}>
        <Button variant="contained" type="submit" disableElevation sx={{ mt: 2 }} onClick={onButtonClick}>
          Mostrar fila de pedidos
        </Button>
      </Grid>

      {totalElements > 0 ? (
        data.map((item) => {
          return (
            <Grid item sx={{ margin: '2rem 0' }} key={item.id}>
              <Box className={classes.card}>
                <Stack direction="row" alignItems="center" spacing={5} className={classes.userInfo}>
                  <Typography sx={{ color: 'white' }}>{item.name}</Typography>
                </Stack>
                <Typography sx={{ padding: '2rem 1rem' }}>
                  <strong>Pedido: </strong>
                  {item.order}
                  <br />
                  {item.description && <strong>Descrição:</strong>} {item.description}
                </Typography>
              </Box>
            </Grid>
          );
        })
      ) : (
        <Grid container alignItems="center" justifyContent="space-around" sx={{ margin: '2rem 0' }}>
          <Typography variant="h6" className={classes.comment}>
            Nenhum pedido encontrado!
          </Typography>
        </Grid>
      )}
    </>
  );
};
