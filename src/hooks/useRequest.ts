/* eslint-disable @typescript-eslint/ban-types */
import axios from 'config/axios';
import { ToastContext } from 'context/ToastContext';
import { useCallback, useContext, useState } from 'react';
import { Orders } from 'types/orders';

interface useApiPostProps {
  url: string;
}
interface IApiPost {
  loading: boolean;
  apiPost: (params?: Orders) => void;
}
interface useApiGetProps {
  url: string;
}
interface IApiGet<T> {
  content: T[];
  totalElements: number;
  loading: boolean;
  fetchOrders: () => void;
}

export function useApiPost({ url }: useApiPostProps): IApiPost {
  const [loading, setLoading] = useState(false);
  const { onMessageSuccess, onMessageError } = useContext(ToastContext);

  const id = Math.floor(Math.random() * (100 - 11)) + 11;

  const apiPost = useCallback(
    async (params = {}) => {
      setLoading(true);
      try {
        await axios.post(url, { id, ...params });
        onMessageSuccess('Pedido realizado com sucesso');
        setLoading(false);
      } catch (e) {
        setLoading(false);
        onMessageError('Não foi possivel realizar o pedido, por favor tente novamente.');
      }
    },
    [id, onMessageError, onMessageSuccess, url]
  );

  return { loading, apiPost };
}

export function useApiGet<T extends {}>({ url }: useApiGetProps): IApiGet<T> {
  const [content, setContent] = useState<T[]>([]);
  const [totalElements, setTotalElements] = useState(0);
  const [loading, setLoading] = useState(false);
  const { onMessageError } = useContext(ToastContext);

  const fetchOrders = useCallback(async () => {
    setLoading(true);
    try {
      const { data: orders } = await axios.get(url);
      setContent(orders.content);
      setTotalElements(orders.totalElements);
      setLoading(false);
    } catch (e) {
      onMessageError('Não foi obter a fila de pedidos, por favor tente novamente.');
    }
  }, [onMessageError, url]);

  return { content, totalElements, loading, fetchOrders };
}
