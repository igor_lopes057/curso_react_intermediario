import Axios from 'axios';

export const cancelSource = Axios;
const axios = Axios.create({ baseURL: process.env.REACT_APP_API_URL });
export default axios;
