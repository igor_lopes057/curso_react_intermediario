export interface Orders {
  name: string;
  lastName: string;
  phone: string;
  order: string;
  description?: string;
}

export interface User {
  id: number;
  name: string;
  lastName: string;
  phone: string;
  order: string;
  description?: string;
}
