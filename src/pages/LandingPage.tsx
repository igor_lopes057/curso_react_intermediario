import React from 'react';
import { Divider, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Header } from 'components/Header';
import { LandingPageComments, LandingPageForm, LandingPageOrders } from 'components/LandingPage';
import LogoPizza1 from 'assets/logo1.jpg';
import LogoPizza2 from 'assets/logo2.jpg';
import { useApiGet, useApiPost } from 'hooks/useRequest';
import { User } from 'types/orders';

const CREATE_ORDERS_URL = '/front/createOrders';
const GET_ORDERS_URL = '/front/findAllOrders';

const useStyles = makeStyles(() => ({
  main: {
    padding: '0 0 2rem',
    margin: '0 1.5rem',
  },
  img: {
    borderRadius: '15px',
    marginTop: '3rem',
  },
  comment: {
    lineHeight: '2.3rem',
    color: '#5b5b5b',
    fontWeight: 600,
  },
}));

const INITIAL_VALUES = {
  name: '',
  lastName: '',
  phone: '',
  order: '',
  description: '',
};

const usersCommentsMock = [
  {
    name: 'Marcelo Dias',
    avatar:
      'https://images.unsplash.com/photo-1599566150163-29194dcaad36?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
    comment: 'Muito boa a pizzaria, recomendo a todos!',
  },
  {
    name: 'Juliana Matos',
    avatar:
      'https://media.istockphoto.com/photos/excited-woman-wearing-rainbow-cardigan-picture-id1327495437?b=1&k=20&m=1327495437&s=170667a&w=0&h=Vbl-XLyAnBoTkyGXXi-X1CFzuSHlNcn-dqB-sCosxFo=',
    comment: '5 estrelas, gosto muito, pertinho de casa',
  },
  {
    name: 'Thiago Gonçalves',
    avatar:
      'https://media.istockphoto.com/photos/young-man-is-playing-with-a-dog-and-do-selfie-picture-id1300658241?b=1&k=20&m=1300658241&s=170667a&w=0&h=0lrTViinfnDjbWDgxV0TDDSAXvzSgmrN-pKq0q60hqA=',
    comment: 'Extrema qualidade',
  },
];

export const LandingPage = (): JSX.Element => {
  const classes = useStyles();
  const { apiPost } = useApiPost({ url: CREATE_ORDERS_URL });
  const { content, totalElements, fetchOrders } = useApiGet<User>({ url: GET_ORDERS_URL });

  return (
    <>
      <Header />
      <main className={classes.main}>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={8} container alignItems="center" sx={{ p: '3rem 2rem 0 0' }}>
            <Typography variant="h4" sx={{ lineHeight: '2.3rem', color: '#5b5b5b', fontWeight: 500, pb: '3rem' }}>
              Faça seu pedido conosco
            </Typography>
            <Typography variant="h6" sx={{ lineHeight: '2.3rem', color: '#5b5b5b' }}>
              Faça o pedido na melhor pizzaria perto de você! Diversos sabores, rapida entrega e com locais acessiveis
              pertinho ai da sua casa! A gente ama o que faz e sabe que o brasileiro é apaixonado por pizzas. Por isso,
              nossa missão é vender pizza com a melhor qualidade e garantindo os melhores ambientes de trabalho para
              nossos funcionários enquanto agrada o seu paladar!
            </Typography>
            <Grid container justifyContent="space-evenly">
              <img className={classes.img} src={LogoPizza1} alt="Pizza" width="300px" />
              <img className={classes.img} src={LogoPizza2} alt="Pedaço de pizza" width="280px" />
            </Grid>
          </Grid>
          <Grid item xs={4}>
            <LandingPageForm onSubmit={apiPost} initialValues={INITIAL_VALUES} />
          </Grid>
        </Grid>

        <Divider sx={{ m: '2rem 0 1rem' }} />
        <Grid container alignItems="center" justifyContent="space-around" sx={{ margin: '2rem 0' }}>
          <LandingPageOrders data={content} totalElements={totalElements} onButtonClick={fetchOrders} />
        </Grid>

        <Divider sx={{ m: '2rem 0 1rem' }} />
        <Typography variant="h6" className={classes.comment}>
          Comentários
        </Typography>

        <Grid container alignItems="center" sx={{ margin: '2rem 0' }}>
          <LandingPageComments users={usersCommentsMock} />
        </Grid>
      </main>
    </>
  );
};
